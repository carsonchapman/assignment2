#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
}

Widget::~Widget()
{
    delete ui;
}

void Widget::setGradeLabel()
{
    int hw1 = ui->spinBox->value();
    int hw2 = ui->spinBox_2->value();
    int hw3 = ui->spinBox_3->value();
    int hw4 = ui->spinBox_4->value();
    int hw5 = ui->spinBox_5->value();
    int hw6 = ui->spinBox_6->value();
    int hw7 = ui->spinBox_7->value();
    int hw8 = ui->spinBox_8->value();
    double totalHwScore = 100 * (hw1 + hw2 + hw3 + hw4 + hw5 + hw6 + hw7 + hw8) / 80;
    int midterm1Score = ui->spinBox_9->value();
    int midterm2Score = ui->spinBox_10->value();
    int finalScore = ui->spinBox_11->value();


    double grade = 0.25 * totalHwScore + 0.2 * (midterm1Score + midterm2Score) + 0.35 * finalScore;
    QString gradeQString = QString::number(grade);
    ui->label_14->setText(gradeQString);
}

void Widget::on_spinBox_valueChanged(int unused)
{
    setGradeLabel();
}

void Widget::on_spinBox_2_valueChanged(int arg1)
{
    setGradeLabel();
}

void Widget::on_spinBox_3_valueChanged(int arg1)
{
    setGradeLabel();
}

void Widget::on_spinBox_4_valueChanged(int arg1)
{
    setGradeLabel();
}

void Widget::on_spinBox_5_valueChanged(int arg1)
{
    setGradeLabel();
}

void Widget::on_spinBox_6_valueChanged(int arg1)
{
    setGradeLabel();
}

void Widget::on_spinBox_7_valueChanged(int arg1)
{
    setGradeLabel();
}

void Widget::on_spinBox_8_valueChanged(int arg1)
{
    setGradeLabel();
}

void Widget::on_spinBox_9_valueChanged(int arg1)
{
    setGradeLabel();
}

void Widget::on_spinBox_10_valueChanged(int arg1)
{
    setGradeLabel();
}

void Widget::on_spinBox_11_valueChanged(int arg1)
{
    setGradeLabel();
}


void Widget::on_radioButton_clicked()
{
    setGradeLabel();
}

void Widget::on_radioButton_2_clicked()
{
    setGradeLabel();
}
